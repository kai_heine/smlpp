#include <catch2/catch.hpp>

#include <smlpp/proc_par_value.h>
using namespace sml;

using vec = std::vector<std::uint8_t>;

TEST_CASE("period entry parsing", "[period_entry]") {
	SECTION("correct") {
		vec v{0x75, 0x02, 0xff, 0x62, 0x01, 0x52, 0xff, 0x42, 0x00, 0x01};
		auto begin = v.begin();
		auto p = parse_period_entry(begin, v.end());
		REQUIRE(p.obj_name == vec{0xff});
		REQUIRE(p.unit_ == unit::year);
		REQUIRE(p.scaler == -1);
		REQUIRE(std::holds_alternative<boolean>(p.value_));
		REQUIRE(!p.value_signature);
	}
	SECTION("wrong type") {
		vec v{0x45};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_period_entry(begin, v.end()), std::system_error);
	}
	SECTION("wrong length") {
		vec v{0x74};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_period_entry(begin, v.end()), std::system_error);
	}
}

TEST_CASE("period entry serialization", "[period_entry]") {
	period_entry p{{0xff}, unit::year, -1, boolean{false}, {}};
	vec v;
	serialize_period_entry(p, std::back_inserter(v));
	REQUIRE(v == vec{0x75, 0x02, 0xff, 0x62, 0x01, 0x52, 0xff, 0x42, 0x00, 0x01});
}

TEST_CASE("procparvalue parsing", "[proc_par_value]") {
	SECTION("value") {
		vec v{0x72, 0x62, 0x01, 0x42, 0x00};
		auto begin = v.begin();
		auto p = parse_proc_par_value(begin, v.end());
		REQUIRE(p.has_value());
		REQUIRE(std::holds_alternative<value>(*p));
	}
	SECTION("period_entry") {
		vec v{0x72, 0x62, 0x02, 0x75, 0x02, 0xff, 0x62, 0x01, 0x52, 0xff, 0x42, 0x00, 0x01};
		auto begin = v.begin();
		auto p = parse_proc_par_value(begin, v.end());
		REQUIRE(p.has_value());
		REQUIRE(std::holds_alternative<period_entry>(*p));
	}
	SECTION("tupel_entry") {
		vec v{0x72, 0x62, 0x03, 0xf1, 0x07,                         //
		      0x02, 0x00, 0x72, 0x62, 0x01, 0x62, 0x00, 0x62, 0x00, //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x02, 0x00,                                           //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		      0x02, 0x00};
		auto begin = v.begin();
		auto t = parse_proc_par_value(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(std::holds_alternative<tupel_entry>(*t));
	}
	SECTION("time") {
		vec v{0x72, 0x62, 0x04, 0x72, 0x62, 0x01, 0x63, 0x12, 0x34};
		auto begin = v.begin();
		auto p = parse_proc_par_value(begin, v.end());
		REQUIRE(p.has_value());
		REQUIRE(std::holds_alternative<sml::time>(*p));
	}
	SECTION("list_entry") {
		vec v{0x72, 0x62, 0x05, 0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01};
		auto begin = v.begin();
		auto p = parse_proc_par_value(begin, v.end());
		REQUIRE(p.has_value());
		REQUIRE(std::holds_alternative<list_entry>(*p));
	}
	SECTION("wrong type") {
		vec v{0x42};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_proc_par_value(begin, v.end()), std::system_error);
	}
	SECTION("wrong length") {
		vec v{0x76};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_proc_par_value(begin, v.end()), std::system_error);
	}
}

TEST_CASE("procparvalue serialization", "[proc_par_value]") {
	SECTION("value") {
		proc_par_value p{value{}};
		vec v;
		serialize_proc_par_value(p, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x62, 0x01, 0x42, 0x00});
	}
	SECTION("period_entry") {
		proc_par_value p{period_entry{{0xff}, unit::year, -1, boolean{false}, {}}};
		vec v;
		serialize_proc_par_value(p, std::back_inserter(v));
		REQUIRE(v ==
		        vec{0x72, 0x62, 0x02, 0x75, 0x02, 0xff, 0x62, 0x01, 0x52, 0xff, 0x42, 0x00, 0x01});
	}
	SECTION("tupel_entry") {
		proc_par_value p{tupel_entry{}};
		std::get<tupel_entry>(p).server_id.resize(1);
		std::get<tupel_entry>(p).signature_pA_r1_r4.resize(1);
		std::get<tupel_entry>(p).signature_mA_r2_r3.resize(1);
		vec v;
		serialize_proc_par_value(p, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x62, 0x03, 0xf1, 0x07,                         //
		                 0x02, 0x00, 0x72, 0x62, 0x01, 0x62, 0x00, 0x62, 0x00, //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x02, 0x00,                                           //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x62, 0x00, 0x52, 0x00, 0x52, 0x00,                   //
		                 0x02, 0x00});
	}
	SECTION("time") {
		proc_par_value p{sml::time{}};
		vec v;
		serialize_proc_par_value(p, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x62, 0x04, 0x72, 0x62, 0x01, 0x62, 0x00});
	}
	SECTION("list_entry") {
		proc_par_value p{list_entry{{0xff}, {}, {}, {}, {}, boolean{false}, {}}};
		vec v;
		serialize_proc_par_value(p, std::back_inserter(v));
		REQUIRE(v ==
		        vec{0x72, 0x62, 0x05, 0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01});
	}
}
