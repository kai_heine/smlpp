#include <catch2/catch.hpp>

#include <smlpp/list.h>
using namespace sml;

using vec = std::vector<std::uint8_t>;

TEST_CASE("list_entry parsing", "[list_entry]") {
	SECTION("optional values") {
		vec v{0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01};
		auto begin = v.begin();
		auto l = parse_list_entry(begin, v.end());
		REQUIRE(l.obj_name == vec{0xff});
		REQUIRE(!l.status_);
		REQUIRE(!l.val_time);
		REQUIRE(!l.unit_);
		REQUIRE(!l.scaler);
		REQUIRE(std::holds_alternative<boolean>(l.value_));
		REQUIRE(!l.value_signature);
	}
	SECTION("not optional") {
		vec v{0x77, 0x02, 0xff, 0x62, 0xff, 0x72, 0x62, 0x01, 0x62,
		      0xff, 0x62, 0x01, 0x52, 0xff, 0x42, 0x00, 0x02, 0xff};
		auto begin = v.begin();
		auto l = parse_list_entry(begin, v.end());
		REQUIRE(l.obj_name == vec{0xff});
		REQUIRE(l.status_);
		REQUIRE(std::holds_alternative<unsigned8>(*l.status_));
		REQUIRE(l.val_time);
		REQUIRE(l.unit_);
		REQUIRE(*l.unit_ == unit::year);
		REQUIRE(l.scaler);
		REQUIRE(*l.scaler == -1);
		REQUIRE(std::holds_alternative<boolean>(l.value_));
		REQUIRE(l.value_signature);
		REQUIRE(l.value_signature->get() == vec{0xff});
	}
}

TEST_CASE("list entry serialization", "[list_entry]") {
	SECTION("not optional") {
		list_entry l{{0xff},       unsigned8{0xff},  sml::time{unsigned32{0xff}}, unit::year, -1,
		             value{false}, signature{{0xff}}};
		vec v;
		serialize_list_entry(l, std::back_inserter(v));
		REQUIRE(v == vec{0x77, 0x02, 0xff, 0x62, 0xff, 0x72, 0x62, 0x01, 0x62, 0xff, 0x62, 0x01,
		                 0x52, 0xff, 0x42, 0x00, 0x02, 0xff});
	}
	SECTION("optional") {
		list_entry l{{0xff}, {}, {}, {}, {}, value{false}, {}};
		vec v;
		serialize_list_entry(l, std::back_inserter(v));
		REQUIRE(v == vec{0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01});
	}
}

TEST_CASE("list parsing", "[list]") {
	vec v{0x72, 0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01,
	      0x77, 0x02, 0xff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01};
	auto begin = v.begin();
	auto l = parse_list(begin, v.end());
	REQUIRE(l.size() == 2);
}

TEST_CASE("list serialization", "[list]") {
	list l{{{0xff}, {}, {}, {}, {}, value{false}, {}}, {{0xff}, {}, {}, {}, {}, value{false}, {}}};
	vec v;
	serialize_list(l, std::back_inserter(v));
	REQUIRE(v == vec{0x72, 0x77, 0x02, 0x0ff, 0x01, 0x01, 0x01, 0x01, 0x42, 0x00, 0x01,
	                 0x77, 0x02, 0xff, 0x01,  0x01, 0x01, 0x01, 0x42, 0x00, 0x01});
}
