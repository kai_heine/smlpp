#include <catch2/catch.hpp>

#include <smlpp/tree.h>
using namespace sml;

using vec = std::vector<std::uint8_t>;

TEST_CASE("tree path parsing", "[tree_path]") {
	SECTION("empty") {
		vec v{0x70};
		auto begin = v.begin();
		auto t = parse_tree_path(begin, v.end());
		REQUIRE(t.path_entries.empty());
	}
	SECTION("not empty") {
		vec v{0x72, 0x02, 0xff, 0x02, 0x00};
		auto begin = v.begin();
		auto t = parse_tree_path(begin, v.end());
		REQUIRE(t.path_entries.size() == 2);
	}
	SECTION("wrong type") {
		vec v{0x42};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_tree_path(begin, v.end()), std::system_error);
	}
}

TEST_CASE("tree path serialization", "[tree_path]") {
	SECTION("empty") {
		tree_path t;
		vec v;
		serialize_tree_path(t, std::back_inserter(v));
		REQUIRE(v == vec{0x70});
	}
	SECTION("not empty") {
		tree_path t{{{0xff}, {0x00}}};
		vec v;
		serialize_tree_path(t, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x02, 0xff, 0x02, 0x00});
	}
}

TEST_CASE("tree parsing", "[tree]") {
	SECTION("one level") {
		vec v{0x73, 0x02, 0x00, 0x01, 0x01};
		auto begin = v.begin();
		auto t = parse_tree(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(!t->child_list.has_value());
	}
	SECTION("two levels") {
		vec v{0x73, 0x02, 0x00, 0x01, 0x71, 0x73, 0x02, 0x00, 0x01, 0x01};
		auto begin = v.begin();
		auto t = parse_tree(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(t->child_list.has_value());
		REQUIRE(t->child_list->size() == 1);
		REQUIRE(!(*t->child_list)[0].child_list.has_value());
	}
}

TEST_CASE("tree serialization", "[tree]") {
	SECTION("one level") {
		tree t{{0x00}, {}, {}};
		vec v;
		serialize_tree(t, std::back_inserter(v));
		REQUIRE(v == vec{0x73, 0x02, 0x00, 0x01, 0x01});
	}
	SECTION("two levels") {
		tree t{octet_string{0x00}, std::nullopt,
		       sequence<tree>{tree{octet_string{0x00}, std::nullopt, std::nullopt}}};
		vec v;
		serialize_tree(t, std::back_inserter(v));
		REQUIRE(v == vec{0x73, 0x02, 0x00, 0x01, 0x71, 0x73, 0x02, 0x00, 0x01, 0x01});
	}
}
