#include <catch2/catch.hpp>

#include <smlpp/time.h>
using namespace sml;

using vec = std::vector<std::uint8_t>;

TEST_CASE("time serialization", "[time]") {
	SECTION("sec_index") {
		sml::time t{unsigned32{0x1234}};
		vec v;
		serialize_time(t, std::back_inserter(v));
		REQUIRE(v.size() == 6);
		REQUIRE(v == vec{0x72, 0x62, 0x01, 0x63, 0x12, 0x34});
	}
	SECTION("timestamp") {
		sml::time t{timestamp{0x1234}};
		vec v;
		serialize_time(t, std::back_inserter(v));
		REQUIRE(v.size() == 6);
		REQUIRE(v == vec{0x72, 0x62, 0x02, 0x63, 0x12, 0x34});
	}
	SECTION("timestamp_local") {
		sml::time t{timestamp_local{timestamp{0x1234}, 0x56, 0x78}};
		vec v;
		serialize_time(t, std::back_inserter(v));
		REQUIRE(v.size() == 11);
		REQUIRE(v == vec{0x72, 0x62, 0x03, 0x73, 0x63, 0x12, 0x34, 0x52, 0x56, 0x52, 0x78});
	}
}

TEST_CASE("time parsing", "[time]") {
	SECTION("sec_index") {
		vec v{0x72, 0x62, 0x01, 0x63, 0x12, 0x34};
		auto begin = v.begin();
		auto t = parse_time(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(std::holds_alternative<unsigned32>(*t));
		REQUIRE(std::get<unsigned32>(*t) == 0x1234);
	}
	SECTION("timestamp") {
		vec v{0x72, 0x62, 0x02, 0x63, 0x12, 0x34};
		auto begin = v.begin();
		auto t = parse_time(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(std::holds_alternative<timestamp>(*t));
		REQUIRE(std::get<timestamp>(*t).get() == 0x1234);
	}
	SECTION("timestamp_local") {
		vec v{0x72, 0x62, 0x03, 0x73, 0x63, 0x12, 0x34, 0x52, 0x56, 0x52, 0x78};
		auto begin = v.begin();
		auto t = parse_time(begin, v.end());
		REQUIRE(t.has_value());
		REQUIRE(std::holds_alternative<timestamp_local>(*t));
		REQUIRE(std::get<timestamp_local>(*t).time_stamp.get() == 0x1234);
		REQUIRE(std::get<timestamp_local>(*t).local_offset == 0x56);
		REQUIRE(std::get<timestamp_local>(*t).season_time_offset == 0x78);
	}
}
