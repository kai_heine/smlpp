#include <catch2/catch.hpp>

#include <smlpp/boolean.h>

using container = std::vector<std::uint8_t>;
using namespace sml;

TEST_CASE("boolean parsing", "[boolean]") {
	SECTION("false") {
		container v{0x42, 0x00};
		auto begin = v.begin();
		auto b = parse_boolean(begin, v.end());
		REQUIRE(b.has_value());
		REQUIRE(*b == false);
	}
	SECTION("true") {
		container v{0x42, 0xff};
		auto begin = v.begin();
		auto b = parse_boolean(begin, v.end());
		REQUIRE(b.has_value());
		REQUIRE(*b == true);

		v[1] = 0x55;
		begin = v.begin();
		b = parse_boolean(begin, v.end());
		REQUIRE(b.has_value());
		REQUIRE(*b == true);
	}
	SECTION("optional") {
		container v{0x01};
		auto begin = v.begin();
		auto b = parse_boolean(begin, v.end());
		REQUIRE(!b.has_value());
	}
	SECTION("wrong type") {
		container v{0x72};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_boolean(begin, v.end()), std::system_error);
	}
	SECTION("wrong length") {
		container v{0x46};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_boolean(begin, v.end()), std::system_error);
	}
	SECTION("buffer too short") {
		container v{0x42};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_boolean(begin, v.end()), std::system_error);
	}
}

TEST_CASE("boolean serialization", "[boolean]") {
	SECTION("false") {
		boolean b = false;
		container v;
		serialize_boolean(b, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x42);
		REQUIRE(v[1] == 0x00);
	}
	SECTION("true") {
		boolean b = true;
		container v;
		serialize_boolean(b, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x42);
		REQUIRE(v[1] != 0x00);
	}
}
