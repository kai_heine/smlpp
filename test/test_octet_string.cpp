#include <catch2/catch.hpp>

#include <smlpp/octet_string.h>

using container = std::vector<std::uint8_t>;
using namespace sml;

TEST_CASE("octet string parsing", "[octet_string]") {
	SECTION("optional") {
		container v{0x01};
		auto begin = v.begin();
		auto o = parse_octet_string(begin, v.end());
		REQUIRE(!o.has_value());
	}
	SECTION("wrong type") {
		container v{0x72};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_octet_string(begin, v.end()), std::system_error);
	}
	SECTION("buffer too short") {
		container v{0x06};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_octet_string(begin, v.end()), std::system_error);
	}
	SECTION("one byte") {
		container v{0x02, 0xff};
		auto begin = v.begin();
		auto o = parse_octet_string(begin, v.end());
		REQUIRE(o.has_value());
		REQUIRE(o->size() == 1);
		REQUIRE(o->front() == 0xff);
	}
	SECTION("14 bytes") {
		container v{0x0f, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
		auto begin = v.begin();
		auto o = parse_octet_string(begin, v.end());
		REQUIRE(o.has_value());
		REQUIRE(o->size() == 14);
		REQUIRE(*o == container{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14});
	}
	SECTION("more than 14 bytes") {
		container v(0x22);
		v[0] = 0x82;
		v[1] = 0x02;
		auto begin = v.begin();
		auto o = parse_octet_string(begin, v.end());
		REQUIRE(o.has_value());
		REQUIRE(o->size() == 0x20);
	}
}

TEST_CASE("octet string serialization", "[octet_string]") {
	SECTION("one byte") {
		octet_string o{0x88};
		container v;
		serialize_octet_string(o, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x02);
		REQUIRE(v[1] == 0x88);
	}
	SECTION("14 bytes") {
		octet_string o{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
		container v;
		serialize_octet_string(o, std::back_inserter(v));
		REQUIRE(v.size() == 15);
		REQUIRE(v == container{0x0f, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14});
	}
	SECTION("more than 14 bytes") {
		octet_string o(0x20);
		container v;
		serialize_octet_string(o, std::back_inserter(v));
		REQUIRE(v.size() == 0x22);
	}
	SECTION("empty octet string") {
		octet_string o;
		container v;
		serialize_octet_string(o, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x01);
	}
}
