#include <catch2/catch.hpp>

#include <smlpp/type_length_field.h>

using container = std::vector<std::uint8_t>;

TEST_CASE("tl field serialization", "[type_length]") {
	using namespace sml;

	SECTION("octet string") {
		SECTION("single byte") {
			type_length tl{type_length::octet_string_type, 10};
			container v;
			serialize_type_length(tl, std::back_inserter(v));
			REQUIRE(v.size() == 1);
			REQUIRE(v[0] == 0x0b);
		}
		SECTION("multi byte") {
			type_length tl{type_length::octet_string_type, 0x1234};
			container v;
			serialize_type_length(tl, std::back_inserter(v));
			REQUIRE(v.size() == 4);
			REQUIRE(v == container{0x81, 0x82, 0x83, 0x08});
		}
		SECTION("multi byte wrap over") {
			type_length tl{type_length::octet_string_type, 0xffff};
			container v;
			serialize_type_length(tl, std::back_inserter(v));
			REQUIRE(v.size() == 5);
			REQUIRE(v == container{0x81, 0x80, 0x80, 0x80, 0x04});
		}
	}
	SECTION("boolean") {
		type_length tl{type_length::boolean_type, 1};
		container v;
		serialize_type_length(tl, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x42);
	}
	SECTION("integer") {
		type_length tl{type_length::integer_type, 8};
		container v;
		serialize_type_length(tl, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x59);
	}
	SECTION("unsigned") {
		type_length tl{type_length::unsigned_type, 3};
		container v;
		serialize_type_length(tl, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x64);
	}
	SECTION("list of") {
		type_length tl{type_length::list_of_type, 7};
		container v;
		serialize_type_length(tl, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x77);
	}
	SECTION("optional") {
		container v;
		serialize_type_length({}, std::back_inserter(v));
		REQUIRE(v.size() == 1);
		REQUIRE(v[0] == 0x01);
	}
}

TEST_CASE("tl field parsing", "[type length]") {
	using namespace sml;

	SECTION("octet string") {
		SECTION("single byte") {
			container v{0x0b};
			auto begin = v.begin();
			auto tl = parse_type_length(begin, v.end());
			REQUIRE(tl.type == type_length::octet_string_type);
			REQUIRE(tl.length == 10);
		}
		SECTION("multi byte") {
			container v{0x81, 0x82, 0x83, 0x08};
			auto begin = v.begin();
			auto tl = parse_type_length(begin, v.end());
			REQUIRE(tl.type == type_length::octet_string_type);
			REQUIRE(tl.length == 0x1234);
		}
	}
	SECTION("boolean") {
		container v{0x42};
		auto begin = v.begin();
		auto tl = parse_type_length(begin, v.end());
		REQUIRE(tl.type == type_length::boolean_type);
		REQUIRE(tl.length == 1);
	}
	SECTION("integer") {
		container v{0x59};
		auto begin = v.begin();
		auto tl = parse_type_length(begin, v.end());
		REQUIRE(tl.type == type_length::integer_type);
		REQUIRE(tl.length == 8);
	}
	SECTION("unsigned") {
		container v{0x64};
		auto begin = v.begin();
		auto tl = parse_type_length(begin, v.end());
		REQUIRE(tl.type == type_length::unsigned_type);
		REQUIRE(tl.length == 3);
	}
	SECTION("list of") {
		container v{0x77};
		auto begin = v.begin();
		auto tl = parse_type_length(begin, v.end());
		REQUIRE(tl.type == type_length::list_of_type);
		REQUIRE(tl.length == 7);
	}
	SECTION("invalid type") {
		container v{0x1f};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_type_length(begin, v.end()), std::system_error);
	}
	SECTION("invalid length bytes") {
		container v{0x81, 0xf2, 0x83, 0x08};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_type_length(begin, v.end()), std::system_error);
	}
}
