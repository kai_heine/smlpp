#include <catch2/catch.hpp>

#include <smlpp/integer.h>

using container = std::vector<std::uint8_t>;
using namespace sml;

TEST_CASE("integer parsing", "[integer]") {
	SECTION("optional") {
		container v{0x01};
		auto begin = v.begin();
		auto i = parse_integer<integer8>(begin, v.end());
		REQUIRE(!i.has_value());
	}
	SECTION("wrong type") {
		container v{0x72};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_integer<integer8>(begin, v.end()), std::system_error);
	}
	SECTION("buffer too short") {
		container v{0x56};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_integer<integer8>(begin, v.end()), std::system_error);
	}
	SECTION("integer8") {
		container v{0x52, 0x41};
		auto begin = v.begin();
		auto i = parse_integer<integer8>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 'A');
	}
	SECTION("integer16") {
		container v{0x53, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<integer16>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x5555);
	}
	SECTION("integer32") {
		container v{0x55, 0x55, 0x55, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<integer32>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x55555555);
	}
	SECTION("integer64") {
		container v{0x59, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<integer64>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x5555555555555555);
	}
	SECTION("unsigned8") {
		container v{0x62, 0x41};
		auto begin = v.begin();
		auto i = parse_integer<unsigned8>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x41);
	}
	SECTION("unsigned16") {
		container v{0x63, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<unsigned16>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x5555);
	}
	SECTION("unsigned32") {
		container v{0x65, 0x55, 0x55, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<unsigned32>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x55555555);
	}
	SECTION("unsigned64") {
		container v{0x69, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55};
		auto begin = v.begin();
		auto i = parse_integer<unsigned64>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x5555555555555555);
	}
	SECTION("wrong signedness") {
		container v{0x62, 0x41};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_integer<integer8>(begin, v.end()), std::system_error);
		v[0] = 0x52;
		REQUIRE_THROWS_AS(parse_integer<unsigned8>(begin, v.end()), std::system_error);
	}
	SECTION("leading zeros can be omitted") {
		container v{0x62, 0x41};
		auto begin = v.begin();
		auto i = parse_integer<unsigned64>(begin, v.end());
		REQUIRE(i.has_value());
		REQUIRE(*i == 0x41);
	}
}

TEST_CASE("integer serialization", "[integer]") {
	SECTION("integer8") {
		integer8 i = 0x55;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x52);
		REQUIRE(v[1] == 0x55);
	}
	SECTION("integer16") {
		integer16 i = 0x5555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 3);
		REQUIRE(v == container{0x53, 0x55, 0x55});
	}
	SECTION("integer32") {
		integer32 i = 0x55555555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 5);
		REQUIRE(v == container{0x55, 0x55, 0x55, 0x55, 0x55});
	}
	SECTION("integer64") {
		integer64 i = 0x5555555555555555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 9);
		REQUIRE(v == container{0x59, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55});
	}
	SECTION("unsigned8") {
		unsigned8 i = 0x55;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x62);
		REQUIRE(v[1] == 0x55);
	}
	SECTION("unsigned16") {
		unsigned16 i = 0x5555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 3);
		REQUIRE(v == container{0x63, 0x55, 0x55});
	}
	SECTION("unsigned32") {
		unsigned32 i = 0x55555555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 5);
		REQUIRE(v == container{0x65, 0x55, 0x55, 0x55, 0x55});
	}
	SECTION("unsigned64") {
		unsigned64 i = 0x5555555555555555;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 9);
		REQUIRE(v == container{0x69, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55});
	}
	SECTION("leading zeros are omitted") {
		unsigned64 i = 1;
		container v;
		serialize_integer(i, std::back_inserter(v));
		REQUIRE(v.size() == 2);
		REQUIRE(v[0] == 0x62);
		REQUIRE(v[1] == 1);
	}
}
