#include <catch2/catch.hpp>

#include <smlpp/list_entry.h>
using namespace sml;

using vec = std::vector<std::uint8_t>;

TEST_CASE("list type parsing", "[list_type]") {
	SECTION("time") {
		vec v{0x72, 0x62, 0x01, 0x72, 0x62, 0x01, 0x62, 0x00};
		auto begin = v.begin();
		auto l = parse_list_type(begin, v.end());
		REQUIRE(std::holds_alternative<sml::time>(l));
	}
	SECTION("timestamped value (unsupported)") {
		vec v{0x72, 0x62, 0x02};
		auto begin = v.begin();
		REQUIRE_THROWS_AS(parse_list_type(begin, v.end()), std::system_error);
	}
	SECTION("cosem value") {
		vec v{0x72, 0x62, 0x03, 0x72, 0x62, 0x01, 0x72, 0x52, 0xff, 0x62, 0x03};
		auto begin = v.begin();
		auto l = parse_list_type(begin, v.end());
		REQUIRE(std::holds_alternative<cosem_value>(l));
		REQUIRE(std::holds_alternative<cosem_scaler_unit_type>(std::get<cosem_value>(l)));
	}
}

TEST_CASE("list type serialization", "[list_type]") {
	SECTION("time") {
		list_type l{sml::time{}};
		vec v;
		serialize_list_type(l, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x62, 0x01, 0x72, 0x62, 0x01, 0x62, 0x00});
	}
	SECTION("timestamped value (unsupported)") {
		list_type l{timestamped_value{}};
		vec v;
		REQUIRE_THROWS_AS(serialize_list_type(l, std::back_inserter(v)), std::system_error);
	}
	SECTION("cosem value") {
		list_type l{cosem_value{cosem_scaler_unit_type{-1, 3}}};
		vec v;
		serialize_list_type(l, std::back_inserter(v));
		REQUIRE(v == vec{0x72, 0x62, 0x03, 0x72, 0x62, 0x01, 0x72, 0x52, 0xff, 0x62, 0x03});
	}
}
