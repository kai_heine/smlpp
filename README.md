# smlpp

Smart Message Language (SML) in modern C++.

## Features

This is a header-only library that implements parsing and serialization of SML files and the SML transport protocol version 1.
It is based on SML version 1.04, but includes some extensions as specified by the FNN for the wired LMN interface.
The following SML message types are currently supported:

- SML_PublicOpen.Req
- SML_PublicOpen.Res
- SML_PublicClose.Req
- SML_PublicClose.Res
- SML_GetProcParameter.Req
- SML_GetProcParameter.Res
- SML_SetProcParameter.Req
- SML_Attention.Res
- SML_GetList.Res

## Requirements

- Requires a compiler that supports C++17.
	- g++ >= 7, clang++ >= 5, VS 2017 >= 15.7
- Tested working with:
	- g++ 7.3.0
	- clang++ 6.0.1 (requires libc++)
	- VS 2017 15.7.5

## Usage

If you use CMake, add this repository to your project (either as a Git submodule or by using 
FetchContent) and add it as a subdirectory:

```CMake
add_subdirectory(path/to/smlpp)
target_link_libraries(target_name PRIVATE smlpp::smlpp)
```

Since this is a header-only library, you can also just copy the `include/` directory to your project.

### Minimal example

```c++
#include <smlpp/file.h>

int main()
{
	std::vector<std::uint8_t> sml_data{ /* ... */ }; // SML data from somewhere

	// parse SML data into data structures
	auto f = sml::parse_file(sml_data.begin(), sml_data.end());

	// do something with the data structures...

	// serialize the data structure
	auto buffer = sml::serialize_file(f);
}
```
