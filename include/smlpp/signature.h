#ifndef SMLPP_SIGNATURE_H
#define SMLPP_SIGNATURE_H

#include "octet_string.h"
#include "types.h"

namespace sml {

using signature = impl::strong_type<octet_string, struct signature_type>;

template <typename InputIterator>
optional<signature> parse_signature(InputIterator& begin, InputIterator end);

} // namespace sml

#include "impl/signature.inl"

#endif // SMLPP_SIGNATURE_H