#ifndef SMLPP_PROC_PAR_VALUE_H
#define SMLPP_PROC_PAR_VALUE_H

#include "integer.h"
#include "list_entry.h"
#include "period_entry.h"
#include "signature.h"
#include "time.h"
#include "types.h"
#include "unit.h"
#include "value.h"

namespace sml {

// yes, they call it tupel instead of tuple.
// no, i did not make this up
struct tupel_entry {
	octet_string server_id;
	time sec_index;
	unsigned64 status;

	unit unit_pA;
	integer8 scaler_pA;
	integer64 value_pA;

	unit unit_r1;
	integer8 scaler_r1;
	integer64 value_r1;

	unit unit_r4;
	integer8 scaler_r4;
	integer64 value_r4;

	octet_string signature_pA_r1_r4;

	unit unit_mA;
	integer8 scaler_mA;
	integer64 value_mA;

	unit unit_r2;
	integer8 scaler_r2;
	integer64 value_r2;

	unit unit_r3;
	integer8 scaler_r3;
	integer64 value_r3;

	octet_string signature_mA_r2_r3;
};
// seriously, who thought that was a good idea?

template <typename InputIterator>
tupel_entry parse_tupel_entry(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_tupel_entry(const tupel_entry& t, OutputIterator dest);

using proc_par_value = choice<value, period_entry, tupel_entry, time, list_entry>;

template <typename InputIterator>
optional<proc_par_value> parse_proc_par_value(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_proc_par_value(const proc_par_value& p, OutputIterator dest);

} // namespace sml

#include "impl/proc_par_value.inl"

#endif // SMLPP_PROC_PAR_VALUE_H