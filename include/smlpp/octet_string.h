#ifndef SMLPP_OCTET_STRING_H
#define SMLPP_OCTET_STRING_H

#include "types.h"

namespace sml {

using octet_string = std::vector<std::uint8_t>;

template <typename InputIterator>
optional<octet_string> parse_octet_string(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_octet_string(const octet_string& o, OutputIterator dest);

} // namespace sml

#include "impl/octet_string.inl"

#endif // SMLPP_OCTET_STRING_H