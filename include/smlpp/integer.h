#ifndef SMLPP_INTEGER_H
#define SMLPP_INTEGER_H

#include "types.h"
#include <cstdint>

namespace sml {

using integer8 = std::int8_t;
using integer16 = std::int16_t;
using integer32 = std::int32_t;
using integer64 = std::int64_t;
using unsigned8 = std::uint8_t;
using unsigned16 = std::uint16_t;
using unsigned32 = std::uint32_t;
using unsigned64 = std::uint64_t;

template <typename Integer, typename InputIterator>
optional<Integer> parse_integer(InputIterator& begin, InputIterator end);

template <typename Integer, typename OutputIterator>
OutputIterator serialize_integer(Integer i, OutputIterator dest);

} // namespace sml

#include "impl/integer.inl"

#endif // SMLPP_INTEGER_H