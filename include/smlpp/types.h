#ifndef SMLPP_TYPES_H
#define SMLPP_TYPES_H

#include <optional>
#include <vector>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244 4267)
#endif

#include <variant>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

namespace sml {

using std::optional;

template <typename... Ts>
using choice = std::variant<Ts...>;

template <typename... Ts>
using implicit_choice = std::variant<Ts...>;

template <typename T>
using sequence = std::vector<T>;

} // namespace sml

#include "impl/types.inl"

#endif // SMLPP_TYPES_H
