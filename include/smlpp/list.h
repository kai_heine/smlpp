#ifndef SMLPP_LIST_H
#define SMLPP_LIST_H

#include "list_entry.h"
#include "types.h"

namespace sml {

using list = sequence<list_entry>;

template <typename InputIterator>
list parse_list(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_list(const list& l, OutputIterator dest);

} // namespace sml

#include "impl/list.inl"

#endif // SMLPP_LIST_H