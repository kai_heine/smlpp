#ifndef SMLPP_PERIOD_ENTRY_H
#define SMLPP_PERIOD_ENTRY_H

#include "integer.h"
#include "octet_string.h"
#include "signature.h"
#include "types.h"
#include "unit.h"
#include "value.h"

namespace sml {

struct period_entry {
	octet_string obj_name;
	unit unit_;
	integer8 scaler;
	value value_;
	optional<signature> value_signature;
};

template <typename InputIterator>
period_entry parse_period_entry(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_period_entry(const period_entry& p, OutputIterator dest);

} // namespace sml

#include "impl/period_entry.inl"

#endif // SMLPP_PERIOD_ENTRY_H
