#ifndef SMLPP_TREE_H
#define SMLPP_TREE_H

#include "octet_string.h"
#include "proc_par_value.h"
#include "types.h"

namespace sml {

struct tree {
	octet_string parameter_name;
	optional<proc_par_value> parameter_value;
	optional<sequence<tree>> child_list;
};

template <typename InputIterator>
optional<tree> parse_tree(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_tree(const tree& t, OutputIterator dest);

struct tree_path {
	sequence<octet_string> path_entries;
};

template <typename InputIterator>
tree_path parse_tree_path(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_tree_path(const tree_path& t, OutputIterator dest);

} // namespace sml

#include "impl/tree.inl"

#endif // SMLPP_TREE_H