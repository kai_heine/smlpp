#ifndef SMLPP_MESSAGE_BODY_H
#define SMLPP_MESSAGE_BODY_H

#include "message_body/attention_response.h"
#include "message_body/get_list_response.h"
#include "message_body/get_proc_parameter_request.h"
#include "message_body/get_proc_parameter_response.h"
#include "message_body/public_close_request.h"
#include "message_body/public_close_response.h"
#include "message_body/public_open_request.h"
#include "message_body/public_open_response.h"
#include "message_body/set_proc_parameter_request.h"
#include "types.h"

namespace sml {

struct get_profile_pack_request {};  // TODO
struct get_profile_pack_response {}; // TODO
struct get_profile_list_request {};  // TODO
struct get_profile_list_response {}; // TODO
struct get_list_request {};          // TODO
struct get_cosem_request {};         // TODO
struct get_cosem_response {};        // TODO
struct set_cosem_request {};         // TODO
struct set_cosem_response {};        // TODO
struct action_cosem_request {};      // TODO
struct action_cosem_response {};     // TODO

using message_body =
    choice<public_open_request, public_open_response, public_close_request, public_close_response,
           get_profile_pack_request, get_profile_pack_response, get_profile_list_request,
           get_profile_list_response, get_proc_parameter_request, get_proc_parameter_response,
           set_proc_parameter_request, get_list_request, get_list_response, get_cosem_request,
           get_cosem_response, set_cosem_request, set_cosem_response, action_cosem_request,
           action_cosem_response, attention_response>;

template <typename InputIterator>
message_body parse_message_body(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_message_body(const message_body& m, OutputIterator dest);

} // namespace sml

#include "impl/message_body.inl"

#endif // SML_MESSAGE_BODY_H
