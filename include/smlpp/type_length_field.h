#ifndef SMLPP_TYPE_LENGTH_FIELD_H
#define SMLPP_TYPE_LENGTH_FIELD_H

#include <cstddef>

namespace sml {

struct type_length {
	enum type_tag : std::uint8_t {
		octet_string_type = 0x00,
		boolean_type = 0x40,
		integer_type = 0x50,
		unsigned_type = 0x60,
		list_of_type = 0x70,
	} type;
	std::size_t length; // actual length of data, excluding type length field itself

	constexpr bool operator==(const type_length& other) {
		return type == other.type && length == other.length;
	}
};

template <typename InputIterator>
type_length parse_type_length(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_type_length(const type_length& tl, OutputIterator dest);

} // namespace sml

#include "impl/type_length_field.inl"

#endif // !SMLPP_TYPE_LENGTH_FIELD_H
