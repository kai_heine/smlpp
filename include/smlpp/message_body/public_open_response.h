#ifndef SMLPP_MESSAGE_BODY_PUBLIC_OPEN_RESPONSE_H
#define SMLPP_MESSAGE_BODY_PUBLIC_OPEN_RESPONSE_H

#include "../integer.h"
#include "../octet_string.h"
#include "../time.h"
#include "../types.h"

namespace sml {

/* PublicOpen.Res */
struct public_open_response {
	optional<octet_string> codepage;
	optional<octet_string> client_id;
	octet_string req_file_id;
	octet_string server_id;
	optional<time> ref_time;
	optional<unsigned8> sml_version;
};

template <typename InputIterator>
public_open_response parse_public_open_response(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_public_open_response(const public_open_response& p, OutputIterator dest);

} // namespace sml

#include "impl/public_open_response.inl"

#endif // SMLPP_MESSAGE_BODY_PUBLIC_OPEN_RESPONSE_H