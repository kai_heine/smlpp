#ifndef SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_REQUEST_H
#define SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_REQUEST_H

#include "../signature.h"
#include "../types.h"

namespace sml {

/* PublicClose.Req */
struct public_close_request {
	optional<signature> global_signature;
};

template <typename InputIterator>
public_close_request parse_public_close_request(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_public_close_request(const public_close_request& p, OutputIterator dest);

} // namespace sml

#include "impl/public_close_request.inl"

#endif // SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_REQUEST_H