#ifndef SMLPP_MESSAGE_BODY_ATTENTION_RESPONSE_H
#define SMLPP_MESSAGE_BODY_ATTENTION_RESPONSE_H

#include "../octet_string.h"
#include "../tree.h"
#include "../types.h"

namespace sml {

struct attention_response {
	octet_string server_id;
	octet_string attention_no;
	optional<octet_string> attention_msg;
	optional<tree> attention_details;
};

template <typename InputIterator>
attention_response parse_attention_response(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_attention_response(const attention_response& a, OutputIterator dest);

} // namespace sml

#include "impl/attention_response.inl"

#endif // SMLPP_MESSAGE_BODY_ATTENTION_RESPONSE_H