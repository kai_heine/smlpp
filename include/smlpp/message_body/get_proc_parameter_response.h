#ifndef SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_RESPONSE_H
#define SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_RESPONSE_H

#include "../octet_string.h"
#include "../tree.h"

namespace sml {

/* GetProcParameter.Res */
struct get_proc_parameter_response {
	octet_string server_id;
	tree_path parameter_tree_path;
	tree parameter_tree;
};

template <typename InputIterator>
get_proc_parameter_response parse_get_proc_parameter_response(InputIterator& begin,
                                                              InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_get_proc_parameter_response(const get_proc_parameter_response& g,
                                                     OutputIterator dest);

} // namespace sml

#include "impl/get_proc_parameter_response.inl"

#endif // SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_RESPONSE_H