#ifndef SMLPP_MESSAGE_BODY_GET_LIST_RESPONSE_H
#define SMLPP_MESSAGE_BODY_GET_LIST_RESPONSE_H

#include "../list.h"
#include "../octet_string.h"
#include "../signature.h"
#include "../time.h"
#include "../types.h"

namespace sml {

/* GetList.Res */
struct get_list_response {
	optional<octet_string> client_id;
	octet_string server_id;
	optional<octet_string> list_name;
	optional<time> act_sensor_time;
	list val_list;
	optional<signature> list_signature;
	optional<time> act_gateway_time;
};

template <typename InputIterator>
get_list_response parse_get_list_response(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_get_list_response(const get_list_response& g, OutputIterator dest);

} // namespace sml

#include "impl/get_list_response.inl"

#endif // SMLPP_MESSAGE_BODY_GET_LIST_RESPONSE_H