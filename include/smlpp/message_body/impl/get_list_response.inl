#ifndef SMLPP_MESSAGE_BODY_IMPL_GET_LIST_RESPONSE_INL
#define SMLPP_MESSAGE_BODY_IMPL_GET_LIST_RESPONSE_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
get_list_response parse_get_list_response(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 7) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	get_list_response glr;
	glr.client_id = parse_octet_string(begin, end);
	glr.server_id = parse_octet_string(begin, end).value_or(octet_string{});
	glr.list_name = parse_octet_string(begin, end);
	glr.act_sensor_time = parse_time(begin, end);
	glr.val_list = parse_list(begin, end);
	glr.list_signature = parse_signature(begin, end);
	glr.act_gateway_time = parse_time(begin, end);

	return glr;
}

template <typename OutputIterator>
OutputIterator serialize_get_list_response(const get_list_response& g, OutputIterator dest) {
	using namespace impl;

	if (g.server_id.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}

	dest = serialize_type_length({type_length::list_of_type, 7}, dest);
	dest = g.client_id ? serialize_octet_string(*g.client_id, dest) : serialize_optional(dest);
	dest = serialize_octet_string(g.server_id, dest);
	dest = g.list_name ? serialize_octet_string(*g.list_name, dest) : serialize_optional(dest);
	dest = g.act_sensor_time ? serialize_time(*g.act_sensor_time, dest) : serialize_optional(dest);
	dest = serialize_list(g.val_list, dest);
	dest = g.list_signature ? serialize_octet_string(g.list_signature->get(), dest)
	                        : serialize_optional(dest);
	dest =
	    g.act_gateway_time ? serialize_time(*g.act_gateway_time, dest) : serialize_optional(dest);

	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_GET_LIST_RESPONSE_INL