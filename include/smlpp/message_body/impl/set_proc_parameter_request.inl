#ifndef SMLPP_MESSAGE_BODY_IMPL_SET_PROC_PARAMETER_REQUEST
#define SMLPP_MESSAGE_BODY_IMPL_SET_PROC_PARAMETER_REQUEST

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
set_proc_parameter_request parse_set_proc_parameter_request(InputIterator& begin,
                                                            InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 5) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	set_proc_parameter_request spr;
	spr.server_id = parse_octet_string(begin, end);
	spr.username = parse_octet_string(begin, end);
	spr.password = parse_octet_string(begin, end);
	spr.parameter_tree_path = parse_tree_path(begin, end);
	spr.parameter_tree = parse_tree(begin, end).value();

	return spr;
}

template <typename OutputIterator>
OutputIterator serialize_set_proc_parameter_request(const set_proc_parameter_request& g,
                                                    OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 5}, dest);
	dest = g.server_id ? serialize_octet_string(*g.server_id, dest) : serialize_optional(dest);
	dest = g.username ? serialize_octet_string(*g.username, dest) : serialize_optional(dest);
	dest = g.password ? serialize_octet_string(*g.password, dest) : serialize_optional(dest);
	dest = serialize_tree_path(g.parameter_tree_path, dest);
	dest = serialize_tree(g.parameter_tree, dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_SET_PROC_PARAMETER_REQUEST