#ifndef SMLPP_MESSAGE_BODY_IMPL_ATTENTION_RESPONSE_INL
#define SMLPP_MESSAGE_BODY_IMPL_ATTENTION_RESPONSE_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
attention_response parse_attention_response(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 4) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	attention_response ar;
	ar.server_id = parse_octet_string(begin, end).value_or(octet_string{});
	ar.attention_no = parse_octet_string(begin, end).value_or(octet_string{});
	ar.attention_msg = parse_octet_string(begin, end);
	ar.attention_details = parse_tree(begin, end);

	return ar;
}

template <typename OutputIterator>
OutputIterator serialize_attention_response(const attention_response& a, OutputIterator dest) {
	if (a.server_id.empty() || a.attention_no.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}

	dest = serialize_type_length({type_length::list_of_type, 4}, dest);
	dest = serialize_octet_string(a.server_id, dest);
	dest = serialize_octet_string(a.attention_no, dest);
	dest = a.attention_msg ? serialize_octet_string(*a.attention_msg, dest)
	                       : impl::serialize_optional(dest);
	dest = a.attention_details ? serialize_tree(*a.attention_details, dest)
	                           : impl::serialize_optional(dest);

	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_ATTENTION_RESPONSE_INL