#ifndef SMLPP_MESSAGE_BODY_IMPL_PUBLIC_OPEN_REQUEST_INL
#define SMLPP_MESSAGE_BODY_IMPL_PUBLIC_OPEN_REQUEST_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
public_open_request parse_public_open_request(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 7) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	public_open_request por;

	por.codepage = parse_octet_string(begin, end);
	por.client_id = parse_octet_string(begin, end).value_or(octet_string{});
	por.req_file_id = parse_octet_string(begin, end).value_or(octet_string{});
	por.server_id = parse_octet_string(begin, end);
	por.username = parse_octet_string(begin, end);
	por.password = parse_octet_string(begin, end);
	por.sml_version = parse_integer<unsigned8>(begin, end);

	return por;
}

template <typename OutputIterator>
OutputIterator serialize_public_open_request(const public_open_request& p, OutputIterator dest) {
	using namespace impl;
	if (p.client_id.empty() || p.req_file_id.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}

	dest = serialize_type_length({type_length::list_of_type, 7}, dest);
	dest = p.codepage ? serialize_octet_string(*p.codepage, dest) : serialize_optional(dest);
	dest = serialize_octet_string(p.client_id, dest);
	dest = serialize_octet_string(p.req_file_id, dest);
	dest = p.server_id ? serialize_octet_string(*p.server_id, dest) : serialize_optional(dest);
	dest = p.username ? serialize_octet_string(*p.username, dest) : serialize_optional(dest);
	dest = p.password ? serialize_octet_string(*p.password, dest) : serialize_optional(dest);
	dest = p.sml_version ? serialize_integer(*p.sml_version, dest) : serialize_optional(dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_PUBLIC_OPEN_REQUEST_INL