#ifndef SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_RESPONSE_INL
#define SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_RESPONSE_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
get_proc_parameter_response parse_get_proc_parameter_response(InputIterator& begin,
                                                              InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 3) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	get_proc_parameter_response gpr;

	gpr.server_id = parse_octet_string(begin, end).value_or(octet_string{});
	gpr.parameter_tree_path = parse_tree_path(begin, end);
	gpr.parameter_tree = parse_tree(begin, end).value();

	return gpr;
}

template <typename OutputIterator>
OutputIterator serialize_get_proc_parameter_response(const get_proc_parameter_response& g,
	OutputIterator dest) {
	dest = serialize_type_length({ type_length::list_of_type, 3 }, dest);
	dest = serialize_octet_string(g.server_id, dest);
	dest = serialize_tree_path(g.parameter_tree_path, dest);
	dest = serialize_tree(g.parameter_tree, dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_RESPONSE_INL