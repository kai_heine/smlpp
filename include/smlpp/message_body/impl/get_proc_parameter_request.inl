#ifndef SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_REQUEST_INL
#define SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_REQUEST_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
get_proc_parameter_request parse_get_proc_parameter_request(InputIterator& begin,
                                                            InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 5) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	get_proc_parameter_request gpr;

	gpr.server_id = parse_octet_string(begin, end);
	gpr.username = parse_octet_string(begin, end);
	gpr.password = parse_octet_string(begin, end);
	gpr.parameter_tree_path = parse_tree_path(begin, end);
	gpr.attribute = parse_octet_string(begin, end);

	return gpr;
}

template <typename OutputIterator>
OutputIterator serialize_get_proc_parameter_request(const get_proc_parameter_request& g,
                                                    OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 5}, dest);
	dest = g.server_id ? serialize_octet_string(*g.server_id, dest) : serialize_optional(dest);
	dest = g.username ? serialize_octet_string(*g.username, dest) : serialize_optional(dest);
	dest = g.password ? serialize_octet_string(*g.password, dest) : serialize_optional(dest);
	dest = serialize_tree_path(g.parameter_tree_path, dest);
	dest = g.attribute ? serialize_octet_string(*g.attribute, dest) : serialize_optional(dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_GET_PROC_PARAMETER_REQUEST_INL