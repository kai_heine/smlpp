#ifndef SMLPP_MESSAGE_BODY_IMPL_PUBLIC_CLOSE_RESPONSE_INL
#define SMLPP_MESSAGE_BODY_IMPL_PUBLIC_CLOSE_RESPONSE_INL

#include "../../error.h"
#include "../../type_length_field.h"

namespace sml {

template <typename InputIterator>
public_close_response parse_public_close_response(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 1) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	public_close_response pcr;
	pcr.global_signature = parse_signature(begin, end);
	return pcr;
}

template <typename OutputIterator>
OutputIterator serialize_public_close_response(const public_close_response& p,
                                               OutputIterator dest) {
	dest = serialize_type_length({type_length::list_of_type, 1}, dest);
	dest = p.global_signature ? serialize_octet_string(p.global_signature->get(), dest)
	                          : impl::serialize_optional(dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_MESSAGE_BODY_IMPL_PUBLIC_CLOSE_RESPONSE_INL