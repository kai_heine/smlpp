#ifndef SMLPP_MESSAGE_BODY_PUBLIC_OPEN_REQUEST_H
#define SMLPP_MESSAGE_BODY_PUBLIC_OPEN_REQUEST_H

#include "../integer.h"
#include "../octet_string.h"
#include "../types.h"

namespace sml {

/* PublicOpen.Req */
struct public_open_request {
	optional<octet_string> codepage;
	octet_string client_id;
	octet_string req_file_id;
	optional<octet_string> server_id;
	optional<octet_string> username;
	optional<octet_string> password;
	optional<unsigned8> sml_version;
};

template <typename InputIterator>
public_open_request parse_public_open_request(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_public_open_request(const public_open_request& p, OutputIterator dest);

} // namespace sml

#include "impl/public_open_request.inl"

#endif // SMLPP_MESSAGE_BODY_PUBLIC_OPEN_REQUEST_H