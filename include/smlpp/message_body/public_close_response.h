#ifndef SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_RESPONSE_H
#define SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_RESPONSE_H

#include "../signature.h"
#include "../types.h"

namespace sml {

/* PublicClose.Res */
struct public_close_response {
	optional<signature> global_signature;
};

template <typename InputIterator>
public_close_response parse_public_close_response(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_public_close_response(const public_close_response& p, OutputIterator dest);

} // namespace sml

#include "impl/public_close_response.inl"

#endif // SMLPP_MESSAGE_BODY_PUBLIC_CLOSE_RESPONSE_H