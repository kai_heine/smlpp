#ifndef SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_REQUEST_H
#define SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_REQUEST_H

#include "../octet_string.h"
#include "../tree.h"
#include "../types.h"

namespace sml {

/* GetProcParameter.Req */
struct get_proc_parameter_request {
	optional<octet_string> server_id;
	optional<octet_string> username;
	optional<octet_string> password;
	tree_path parameter_tree_path;
	optional<octet_string> attribute;
};

template <typename InputIterator>
get_proc_parameter_request parse_get_proc_parameter_request(InputIterator& begin,
                                                            InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_get_proc_parameter_request(const get_proc_parameter_request& g,
                                                    OutputIterator dest);

} // namespace sml

#include "impl/get_proc_parameter_request.inl"

#endif // SMLPP_MESSAGE_BODY_GET_PROC_PARAMETER_REQUEST_H