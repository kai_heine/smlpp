#ifndef SMLPP_FILE_H
#define SMLPP_FILE_H

#include "message.h"
#include "types.h"

namespace sml {

struct file {
	sequence<message> messages;
};

template <bool use_transport_protocol = true, typename InputIterator>
file parse_file(InputIterator begin, InputIterator end);

template <bool use_transport_protocol = true>
std::vector<std::uint8_t> serialize_file(const file& f);

template <bool use_transport_protocol = true, typename OutputIterator>
OutputIterator serialize_file(const file& f, OutputIterator dest);

} // namespace sml

#include "impl/file.inl"

#endif // SMLPP_FILE_H
