#ifndef SMLPP_LIST_ENTRY_H
#define SMLPP_LIST_ENTRY_H

#include "integer.h"
#include "signature.h"
#include "time.h"
#include "types.h"
#include "unit.h"
#include "value.h"

namespace sml {

using status = implicit_choice<unsigned8, unsigned16, unsigned32, unsigned64>;

template <typename InputIterator>
optional<status> parse_status(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_status(status s, OutputIterator dest);

struct list_entry {
	octet_string obj_name;
	optional<status> status_;
	optional<time> val_time;
	optional<unit> unit_;
	optional<integer8> scaler;
	value value_;
	optional<signature> value_signature;
};

template <typename InputIterator>
list_entry parse_list_entry(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_list_entry(const list_entry& l, OutputIterator dest);

} // namespace sml

#include "impl/list_entry.inl"

#endif // SMLPP_LIST_ENTRY_H
