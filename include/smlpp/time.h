#ifndef SMLPP_TIME_H
#define SMLPP_TIME_H

#include "integer.h"
#include "types.h"

namespace sml {

using timestamp = impl::strong_type<unsigned32, struct timestamp_type>;

template <typename InputIterator>
optional<timestamp> parse_timestamp(InputIterator& begin, InputIterator end);

struct timestamp_local {
	timestamp time_stamp;
	integer16 local_offset;
	integer16 season_time_offset;
};

template <typename InputIterator>
optional<timestamp_local> parse_timestamp_local(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_timestamp_local(const timestamp_local& t, OutputIterator dest);

using time = choice<unsigned32, timestamp, timestamp_local>;

template <typename InputIterator>
optional<time> parse_time(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_time(const time& t, OutputIterator dest);

} // namespace sml

#include "impl/time.inl"

#endif // SMLPP_TIME_H
