#ifndef SMLPP_BOOLEAN_H
#define SMLPP_BOOLEAN_H

#include "types.h"

namespace sml {

using boolean = bool;

template <typename InputIterator>
optional<boolean> parse_boolean(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_boolean(boolean b, OutputIterator dest);

} // namespace sml

#include "impl/boolean.inl"

#endif // SMLPP_BOOLEAN_H