#ifndef SMLPP_MESSAGE_H
#define SMLPP_MESSAGE_H

#include "integer.h"
#include "message_body.h"
#include "octet_string.h"

namespace sml {

struct message {
	octet_string transaction_id;
	unsigned8 group_no;
	unsigned8 abort_on_error;
	message_body body;
	// checksum and end of message are automatically checked/generated
};

template <typename InputIterator>
message parse_message(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_message(const message& m, OutputIterator dest);

} // namespace sml

#include "impl/message.inl"

#endif // SMLPP_MESSAGE_H
