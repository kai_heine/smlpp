#ifndef SMLPP_ERROR_H
#define SMLPP_ERROR_H

#include <system_error>

namespace sml {

enum class errc {
	invalid_type = 1,
	invalid_length,
	unexpected_type,
	unexpected_length,
	short_buffer,
	unsupported_message_body_choice,
	unsupported_time_choice,
	unsupported_proc_par_value_choice,
	unsupported_list_type_choice,
	unsupported_cosem_value_choice,
	octet_string_empty_but_not_optional,
	incorrect_crc,
	invalid_transport_protocol
};

inline std::error_code make_error_code(errc e);

} // namespace sml

#include "impl/error.inl"

#endif // SMLPP_ERROR_H
