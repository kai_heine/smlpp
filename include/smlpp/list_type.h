#ifndef SMLPP_LIST_TYPE_H
#define SMLPP_LIST_TYPE_H

#include "integer.h"
#include "time.h"
#include "types.h"

namespace sml {

struct timestamped_value {}; // TODO

struct cosem_scaler_unit_type {
	integer8 scaler;
	unsigned8 unit;
};

template <typename InputIterator>
cosem_scaler_unit_type parse_cosem_scaler_unit_type(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_cosem_scaler_unit_type(const cosem_scaler_unit_type& c,
                                                OutputIterator dest);

using cosem_value = choice<cosem_scaler_unit_type>; // other choices omitted

template <typename InputIterator>
cosem_value parse_cosem_value(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_cosem_value(const cosem_value& c, OutputIterator dest);

using list_type = choice<time, timestamped_value, cosem_value>;

template <typename InputIterator>
list_type parse_list_type(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_list_type(const list_type& l, OutputIterator dest);

} // namespace sml

#include "impl/list_type.inl"

#endif // SMLPP_LIST_TYPE_H