#ifndef SMLPP_IMPL_LIST_ENTRY_INL
#define SMLPP_IMPL_LIST_ENTRY_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
optional<status> parse_status(InputIterator& begin, InputIterator end) {
	auto old_begin = begin;
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	begin = old_begin; // implicit choice, no tag

	if (tl.length > sizeof(unsigned32))
		return parse_integer<unsigned64>(begin, end).value();
	if (tl.length > sizeof(unsigned16))
		return parse_integer<unsigned32>(begin, end).value();
	if (tl.length > sizeof(unsigned8))
		return parse_integer<unsigned16>(begin, end).value();
	return parse_integer<unsigned8>(begin, end).value();
}

template <typename OutputIterator>
OutputIterator serialize_status(status s, OutputIterator dest) {
	return std::visit([&](auto status_) { return serialize_integer(status_, dest); }, s);
}

template <typename InputIterator>
list_entry parse_list_entry(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 7) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	list_entry le;
	le.obj_name = parse_octet_string(begin, end).value_or(octet_string{});
	le.status_ = parse_status(begin, end);
	le.val_time = parse_time(begin, end);
	le.unit_ = parse_integer<unit>(begin, end);
	le.scaler = parse_integer<integer8>(begin, end);
	le.value_ = parse_value(begin, end);
	le.value_signature = parse_signature(begin, end);

	return le;
}

template <typename OutputIterator>
OutputIterator serialize_list_entry(const list_entry& l, OutputIterator dest) {
	using namespace impl;

	if (l.obj_name.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}

	dest = serialize_type_length({type_length::list_of_type, 7}, dest);
	dest = serialize_octet_string(l.obj_name, dest);
	dest = l.status_ ? serialize_status(*l.status_, dest) : serialize_optional(dest);
	dest = l.val_time ? serialize_time(*l.val_time, dest) : serialize_optional(dest);
	dest = l.unit_ ? serialize_integer(enum_val(*l.unit_), dest) : serialize_optional(dest);
	dest = l.scaler ? serialize_integer(*l.scaler, dest) : serialize_optional(dest);
	dest = serialize_value(l.value_, dest);
	dest = l.value_signature ? serialize_octet_string(l.value_signature->get(), dest)
	                         : serialize_optional(dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_LIST_ENTRY_INL
