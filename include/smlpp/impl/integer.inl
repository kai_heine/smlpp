#ifndef SMLPP_IMPL_INTEGER_INL
#define SMLPP_IMPL_INTEGER_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename Integer, typename InputIterator>
optional<Integer> parse_integer(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}

	if (!(tl.type == type_length::integer_type || tl.type == type_length::unsigned_type) ||
	    (std::is_signed_v<Integer> && (tl.type == type_length::unsigned_type)) ||
	    (std::is_unsigned_v<Integer> && (tl.type == type_length::integer_type))) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (std::distance(begin, end) < static_cast<std::ptrdiff_t>(tl.length)) {
		throw std::system_error(errc::short_buffer, __func__);
	}

	unsigned64 value{0};
	for (auto next = begin + tl.length; begin != next; ++begin) {
		value = (value << 8) + *begin;
	}
	return static_cast<Integer>(value);
}

namespace impl {

template <typename Integer>
constexpr std::size_t min_num_bytes(Integer i) noexcept {
	std::size_t length{0};
	if constexpr (sizeof(i) == 1) {
		length = 1;
	} else {
		auto val = i;
		do {
			val >>= 8;
			++length;
		} while (val != 0 && length < sizeof(Integer));
	}
	return length;
}

} // namespace impl

template <typename Integer, typename OutputIterator>
OutputIterator serialize_integer(Integer i, OutputIterator dest) {
	auto type = std::is_signed_v<Integer> ? type_length::integer_type : type_length::unsigned_type;
	auto length = impl::min_num_bytes(i);

	dest = serialize_type_length({type, length}, dest);

	for (std::size_t count = 0; count < length; ++count) {
		*dest++ = (i >> (8 * (length - 1 - count))) & 0xff;
	}

	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_INTEGER_INL
