#ifndef SMLPP_IMPL_OCTET_STRING_INL
#define SMLPP_IMPL_OCTET_STRING_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
optional<octet_string> parse_octet_string(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::octet_string_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (std::distance(begin, end) < static_cast<std::ptrdiff_t>(tl.length)) {
		throw std::system_error(errc::short_buffer, __func__);
	}

	octet_string ret(begin, begin + tl.length);
	begin += tl.length;
	return ret;
}

template <typename OutputIterator>
OutputIterator serialize_octet_string(const octet_string& o, OutputIterator dest) {
	dest = serialize_type_length({type_length::octet_string_type, o.size()}, dest);
	dest = std::copy(o.begin(), o.end(), dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_OCTET_STRING_INL