#ifndef SMLPP_IMPL_PERIOD_ENTRY_INL
#define SMLPP_IMPL_PERIOD_ENTRY_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
period_entry parse_period_entry(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 5) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	period_entry pe;
	pe.obj_name = parse_octet_string(begin, end).value_or(octet_string{});
	pe.unit_ = parse_integer<unit>(begin, end).value();
	pe.scaler = parse_integer<integer8>(begin, end).value();
	pe.value_ = parse_value(begin, end);
	pe.value_signature = parse_signature(begin, end);

	return pe;
}

template <typename OutputIterator>
OutputIterator serialize_period_entry(const period_entry& p, OutputIterator dest) {
	using namespace impl;

	if (p.obj_name.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}
	dest = serialize_type_length({type_length::list_of_type, 5}, dest);
	dest = serialize_octet_string(p.obj_name, dest);
	dest = serialize_integer(enum_val(p.unit_), dest);
	dest = serialize_integer(p.scaler, dest);
	dest = serialize_value(p.value_, dest);
	dest = p.value_signature ? serialize_octet_string(p.value_signature->get(), dest)
	                         : serialize_optional(dest);
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_PERIOD_ENTRY_INL
