#ifndef SMLPP_IMPL_VALUE_INL
#define SMLPP_IMPL_VALUE_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
value parse_value(InputIterator& begin, InputIterator end) {
	auto old_begin = begin;
	auto [type, length] = parse_type_length(begin, end);
	begin = old_begin; // implicit choice, no tag
	switch (type) {
	case type_length::boolean_type:
		return parse_boolean(begin, end).value();
	case type_length::octet_string_type:
		return parse_octet_string(begin, end).value();
	case type_length::integer_type: {
		if (length > sizeof(integer32))
			return parse_integer<integer64>(begin, end).value();
		if (length > sizeof(integer16))
			return parse_integer<integer32>(begin, end).value();
		if (length > sizeof(integer8))
			return parse_integer<integer16>(begin, end).value();
		return parse_integer<integer8>(begin, end).value();
	}
	case type_length::unsigned_type: {
		if (length > sizeof(unsigned32))
			return parse_integer<unsigned64>(begin, end).value();
		if (length > sizeof(unsigned16))
			return parse_integer<unsigned32>(begin, end).value();
		if (length > sizeof(unsigned8))
			return parse_integer<unsigned16>(begin, end).value();
		return parse_integer<unsigned8>(begin, end).value();
	}
	case type_length::list_of_type:
		return parse_list_type(begin, end);
	default:
		// unreachable
		throw std::system_error(errc::invalid_type, __func__);
		break;
	}
}

template <typename OutputIterator>
OutputIterator serialize_value(const value& v, OutputIterator dest) {
	using namespace impl;
	return std::visit(
	    visitor{[&](boolean b) { return serialize_boolean(b, dest); },
	            [&](const octet_string& o) { return serialize_octet_string(o, dest); },
	            [&](const list_type& l) { return serialize_list_type(l, dest); },
	            [&](auto i) { return serialize_integer(i, dest); }},
	    v);
}

} // namespace sml

#endif // SMLPP_IMPL_VALUE_INL