#ifndef SMLPP_IMPL_MESSAGE_INL
#define SMLPP_IMPL_MESSAGE_INL

#include "../crc16.h"
#include "../error.h"
#include "../type_length_field.h"
#include <iterator>

namespace sml {

namespace impl {
constexpr std::uint8_t END_OF_SML_MESSAGE{0x00};
}

template <typename InputIterator>
message parse_message(InputIterator& begin, InputIterator end) {
	auto crc_begin = begin;

	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 6) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	message msg;
	msg.transaction_id = parse_octet_string(begin, end).value_or(octet_string{});
	msg.group_no = parse_integer<unsigned8>(begin, end).value();
	msg.abort_on_error = parse_integer<unsigned8>(begin, end).value();
	msg.body = parse_message_body(begin, end);

	auto crc_end = begin;
	auto crc = parse_integer<unsigned16>(begin, end).value();
	if (crc != crc16(crc_begin, crc_end)) {
		throw std::system_error(errc::incorrect_crc, __func__);
	}

	if (*begin == impl::END_OF_SML_MESSAGE) {
		++begin;
	} // else throw?
	return msg;
}

template <typename OutputIterator>
OutputIterator serialize_message(const message& m, OutputIterator dest) {
	if (m.transaction_id.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}

	// temporary buffer to simplify crc calculation
	std::vector<std::uint8_t> tmp;
	auto tmp_dest = std::back_inserter(tmp);

	serialize_type_length({type_length::list_of_type, 6}, tmp_dest);
	serialize_octet_string(m.transaction_id, tmp_dest);
	serialize_integer(m.group_no, tmp_dest);
	serialize_integer(m.abort_on_error, tmp_dest);
	serialize_message_body(m.body, tmp_dest);

	dest = std::copy(tmp.begin(), tmp.end(), dest);
	dest = serialize_integer(crc16(tmp.begin(), tmp.end()), dest);
	*dest++ = impl::END_OF_SML_MESSAGE;

	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_MESSAGE_INL
