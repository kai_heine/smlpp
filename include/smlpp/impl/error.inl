#ifndef SMLPP_IMPL_ERROR_INL
#define SMLPP_IMPL_ERROR_INL

namespace sml {

namespace {

struct sml_error_category : std::error_category {
	const char* name() const noexcept override { return "SML"; }

	std::string message(int ev) const override {
		switch (static_cast<errc>(ev)) {
		case errc::invalid_type:
			return "invalid type in tl field";
		case errc::invalid_length:
			return "invalid length bytes in tl field";
		case errc::unexpected_type:
			return "unexpected type";
		case errc::unexpected_length:
			return "unexpected length";
		case errc::short_buffer:
			return "buffer too short";
		case errc::unsupported_message_body_choice:
			return "unsupported SML_MessageBody choice";
		case errc::unsupported_time_choice:
			return "unsupported SML_Time choice";
		case errc::unsupported_proc_par_value_choice:
			return "unsupported SML_ProcParValue choice";
		case errc::unsupported_cosem_value_choice:
			return "unsupported SML_CosemValue choice";
		case errc::unsupported_list_type_choice:
			return "unsupported SML_ListType choice";
		case errc::octet_string_empty_but_not_optional:
			return "octet string empty but not optional (must have at least one element)";
		case errc::incorrect_crc:
			return "incorrect crc";
		case errc::invalid_transport_protocol:
			return "invalid transport protocol";
		default:
			return "unknown error";
		}
	}
};

inline const sml_error_category sml_error_category_object{};

} // namespace

inline std::error_code make_error_code(errc e) {
	return {static_cast<int>(e), sml_error_category_object};
}

} // namespace sml

namespace std {
template <>
struct is_error_code_enum<sml::errc> : true_type {};
} // namespace std

#endif // SMLPP_IMPL_ERROR_INL