#ifndef SMLPP_PROC_PAR_VALUE_INL
#define SMLPP_PROC_PAR_VALUE_INL

#include "../error.h"
#include "../integer.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
tupel_entry parse_tupel_entry(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 23) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	tupel_entry t;
	t.server_id = parse_octet_string(begin, end).value_or(octet_string{});
	t.sec_index = parse_time(begin, end).value();
	t.status = parse_integer<unsigned64>(begin, end).value();
	t.unit_pA = parse_integer<unit>(begin, end).value();
	t.scaler_pA = parse_integer<integer8>(begin, end).value();
	t.value_pA = parse_integer<integer64>(begin, end).value();
	t.unit_r1 = parse_integer<unit>(begin, end).value();
	t.scaler_r1 = parse_integer<integer8>(begin, end).value();
	t.value_r1 = parse_integer<integer64>(begin, end).value();
	t.unit_r4 = parse_integer<unit>(begin, end).value();
	t.scaler_r4 = parse_integer<integer8>(begin, end).value();
	t.value_r4 = parse_integer<integer64>(begin, end).value();
	t.signature_pA_r1_r4 = parse_octet_string(begin, end).value_or(octet_string{});
	t.unit_mA = parse_integer<unit>(begin, end).value();
	t.scaler_mA = parse_integer<integer8>(begin, end).value();
	t.value_mA = parse_integer<integer64>(begin, end).value();
	t.unit_r2 = parse_integer<unit>(begin, end).value();
	t.scaler_r2 = parse_integer<integer8>(begin, end).value();
	t.value_r2 = parse_integer<integer64>(begin, end).value();
	t.unit_r3 = parse_integer<unit>(begin, end).value();
	t.scaler_r3 = parse_integer<integer8>(begin, end).value();
	t.value_r3 = parse_integer<integer64>(begin, end).value();
	t.signature_mA_r2_r3 = parse_octet_string(begin, end).value_or(octet_string{});

	return t;
}

template <typename OutputIterator>
OutputIterator serialize_tupel_entry(const tupel_entry& t, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 23}, dest);
	dest = serialize_octet_string(t.server_id, dest);
	dest = serialize_time(t.sec_index, dest);
	dest = serialize_integer(t.status, dest);
	dest = serialize_integer(enum_val(t.unit_pA), dest);
	dest = serialize_integer(t.scaler_pA, dest);
	dest = serialize_integer(t.value_pA, dest);
	dest = serialize_integer(enum_val(t.unit_r1), dest);
	dest = serialize_integer(t.scaler_r1, dest);
	dest = serialize_integer(t.value_r1, dest);
	dest = serialize_integer(enum_val(t.unit_r4), dest);
	dest = serialize_integer(t.scaler_r4, dest);
	dest = serialize_integer(t.value_r4, dest);
	dest = serialize_octet_string(t.signature_pA_r1_r4, dest);
	dest = serialize_integer(enum_val(t.unit_mA), dest);
	dest = serialize_integer(t.scaler_mA, dest);
	dest = serialize_integer(t.value_mA, dest);
	dest = serialize_integer(enum_val(t.unit_r2), dest);
	dest = serialize_integer(t.scaler_r2, dest);
	dest = serialize_integer(t.value_r2, dest);
	dest = serialize_integer(enum_val(t.unit_r3), dest);
	dest = serialize_integer(t.scaler_r3, dest);
	dest = serialize_integer(t.value_r3, dest);
	dest = serialize_octet_string(t.signature_mA_r2_r3, dest);
	return dest;
}

namespace impl {
enum class proc_par_value_tag : unsigned8 {
	value = 0x01,
	period_entry = 0x02,
	tupel_entry = 0x03,
	time = 0x04,
	list_entry = 0x05
};
}

template <typename InputIterator>
optional<proc_par_value> parse_proc_par_value(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (tl.length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	using impl::proc_par_value_tag;
	auto tag = parse_integer<proc_par_value_tag>(begin, end).value();
	switch (tag) {
	case proc_par_value_tag::value:
		return parse_value(begin, end);
	case proc_par_value_tag::period_entry:
		return parse_period_entry(begin, end);
	case proc_par_value_tag::tupel_entry:
		return parse_tupel_entry(begin, end);
	case proc_par_value_tag::time:
		return parse_time(begin, end);
	case proc_par_value_tag::list_entry:
		return parse_list_entry(begin, end);
	default:
		throw std::system_error(errc::unsupported_proc_par_value_choice, __func__);
	}
}

template <typename OutputIterator>
OutputIterator serialize_proc_par_value(const proc_par_value& ppv, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 2}, dest);
	return std::visit(
	    visitor{

	        [&](const value& v) {
		        dest = serialize_integer(enum_val(proc_par_value_tag::value), dest);
		        return serialize_value(v, dest);
	        },
	        [&](const period_entry& pe) {
		        dest = serialize_integer(enum_val(proc_par_value_tag::period_entry), dest);
		        return serialize_period_entry(pe, dest);
	        },
	        [&](const tupel_entry& te) {
		        dest = serialize_integer(enum_val(proc_par_value_tag::tupel_entry), dest);
		        return serialize_tupel_entry(te, dest);
	        },
	        [&](const time& t) {
		        dest = serialize_integer(enum_val(proc_par_value_tag::time), dest);
		        return serialize_time(t, dest);
	        },
	        [&](const list_entry& l) {
		        dest = serialize_integer(enum_val(proc_par_value_tag::list_entry), dest);
		        return serialize_list_entry(l, dest);
	        },
	        [&](auto) -> OutputIterator {
		        throw std::system_error(errc::unsupported_proc_par_value_choice, __func__);
	        }},
	    ppv);
}

} // namespace sml

#endif // SMLPP_PROC_PAR_VALUE_INL
