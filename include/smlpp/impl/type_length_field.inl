#ifndef SMLPP_TYPE_LENGTH_FIELD_INL
#define SMLPP_TYPE_LENGTH_FIELD_INL

#include "../error.h"
#include <cassert>

namespace sml {

namespace impl {
inline const type_length optional_type_length{};

template <typename OutputIterator>
OutputIterator serialize_optional(OutputIterator dest) {
	return *dest++ = 0x01;
}
} // namespace impl

template <typename InputIterator>
type_length parse_type_length(InputIterator& begin, InputIterator end) {
	assert(begin < end);

	enum bit_masks : std::uint8_t {
		msb_mask = 0b1000'0000,
		type_mask = 0b0111'0000,
		length_mask = 0b0000'1111
	};

	auto tl_field = *begin;

	auto type = static_cast<type_length::type_tag>(tl_field & type_mask);
	if (!(type == type_length::octet_string_type || type == type_length::boolean_type ||
	      type == type_length::integer_type || type == type_length::unsigned_type ||
	      type == type_length::list_of_type)) {
		throw std::system_error(errc::invalid_type, __func__);
	}

	auto length = static_cast<std::size_t>(tl_field & length_mask);
	std::size_t num_bytes = 1;

	while ((begin != end) && ((*begin & msb_mask) != 0)) {
		tl_field = *(++begin);
		++num_bytes;
		if ((tl_field & type_mask) != 0) {
			throw std::system_error(errc::invalid_length, __func__);
		}
		length = (length << 4) + (tl_field & length_mask);
	}
	// for boolean, integer, unsigned and octet string, the length includes the tl field
	if (type != type_length::list_of_type) {
		length -= num_bytes;
	}
	++begin; // advance to next field
	return {type, length};
}

template <typename OutputIterator>
OutputIterator serialize_type_length(const type_length& tl, OutputIterator dest) {
	assert(tl.type == type_length::octet_string_type || tl.type == type_length::boolean_type ||
	       tl.type == type_length::integer_type || tl.type == type_length::unsigned_type ||
	       tl.type == type_length::list_of_type);

	auto length_ = tl.length;
	auto type_ = static_cast<std::uint8_t>(tl.type);
	std::size_t num_bytes{1};

	for (auto l = length_; l > 0xf;) {
		l = (l >> 4);
		++num_bytes;
	}

	if (tl.type != type_length::list_of_type) {
		length_ += num_bytes;
		if ((length_ >> (4 * num_bytes)) > 0) {
			++num_bytes;
			++length_;
		}
	}

	if (num_bytes == 1) {
		*dest++ = static_cast<std::uint8_t>(type_ | length_);
		return dest;
	}

	for (std::size_t i = 0; i < num_bytes; ++i) {
		std::uint8_t byte = (length_ >> (4 * (num_bytes - 1 - i))) & 0x0f;
		if (i == 0) { // first byte contains type
			byte |= type_;
		}
		if (i < (num_bytes - 1)) { // msb signifies additional bytes
			byte |= 0x80;
		}
		*dest++ = byte;
	}

	return dest;
}

} // namespace sml

#endif // SMLPP_TYPE_LENGTH_FIELD_INL
