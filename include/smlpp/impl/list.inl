#ifndef SMLPP_IMPL_LIST_INL
#define SMLPP_IMPL_LIST_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
list parse_list(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}

	list l;
	for (std::size_t i = 0; i < length; ++i) {
		l.push_back(parse_list_entry(begin, end));
	}
	return l;
}

template <typename OutputIterator>
OutputIterator serialize_list(const list& l, OutputIterator dest) {
	dest = serialize_type_length({type_length::list_of_type, l.size()}, dest);
	for (const auto& entry : l) {
		dest = serialize_list_entry(entry, dest);
	}
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_LIST_INL