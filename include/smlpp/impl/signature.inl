#ifndef SMLPP_IMPL_SIGNATURE_INL
#define SMLPP_IMPL_SIGNATURE_INL

#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
optional<signature> parse_signature(InputIterator& begin, InputIterator end) {
	return optional<signature>{parse_octet_string(begin, end)};
}

} // namespace sml

#endif // SMLPP_IMPL_SIGNATURE_INL