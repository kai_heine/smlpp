#ifndef SMLPP_IMPL_LIST_TYPE_INL
#define SMLPP_IMPL_LIST_TYPE_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
cosem_scaler_unit_type parse_cosem_scaler_unit_type(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	cosem_scaler_unit_type csut;
	csut.scaler = parse_integer<integer8>(begin, end).value();
	csut.unit = parse_integer<unsigned8>(begin, end).value();

	return csut;
}

template <typename OutputIterator>
OutputIterator serialize_cosem_scaler_unit_type(const cosem_scaler_unit_type& c,
                                                OutputIterator dest) {
	dest = serialize_type_length({type_length::list_of_type, 2}, dest);
	dest = serialize_integer(c.scaler, dest);
	dest = serialize_integer(c.unit, dest);
	return dest;
}

namespace impl {
enum class cosem_value_tag : unsigned8 { cosem_scaler_unit_type = 0x01 };
}

template <typename InputIterator>
cosem_value parse_cosem_value(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	using impl::cosem_value_tag;
	auto tag = parse_integer<cosem_value_tag>(begin, end).value();
	switch (tag) {
	case cosem_value_tag::cosem_scaler_unit_type:
		return parse_cosem_scaler_unit_type(begin, end);
	default:
		throw std::system_error(errc::unsupported_cosem_value_choice, __func__);
	}
}

template <typename OutputIterator>
OutputIterator serialize_cosem_value(const cosem_value& c, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 2}, dest);

	return std::visit(
	    visitor{

	        [&](const cosem_scaler_unit_type& csut) {
		        dest = serialize_integer(enum_val(cosem_value_tag::cosem_scaler_unit_type), dest);
		        return serialize_cosem_scaler_unit_type(csut, dest);
	        },
	        [](auto) -> OutputIterator {
		        throw std::system_error(errc::unsupported_cosem_value_choice, __func__);
	        }},
	    c);
}

namespace impl {
enum class list_type_tag : unsigned8 { time = 0x01, timestamped_value = 0x02, cosem_value = 0x03 };
}

template <typename InputIterator>
list_type parse_list_type(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	using impl::list_type_tag;
	auto tag = parse_integer<list_type_tag>(begin, end).value();
	switch (tag) {
	case list_type_tag::time:
		return parse_time(begin, end).value();
	case list_type_tag::cosem_value:
		return parse_cosem_value(begin, end);
		// TODO: timestamped_value
	default:
		throw std::system_error(errc::unsupported_list_type_choice, __func__);
	}
}

template <typename OutputIterator>
OutputIterator serialize_list_type(const list_type& l, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 2}, dest);

	return std::visit(
	    visitor{

	        [&](const time& t) {
		        dest = serialize_integer(enum_val(list_type_tag::time), dest);
		        return serialize_time(t, dest);
	        },
	        [&](const cosem_value& c) {
		        dest = serialize_integer(enum_val(list_type_tag::cosem_value), dest);
		        return serialize_cosem_value(c, dest);
	        },
	        // TODO: timestamped_value
	        [&](auto) -> OutputIterator {
		        throw std::system_error(errc::unsupported_list_type_choice, __func__);
	        }},
	    l);
}

} // namespace sml

#endif // SMLPP_IMPL_LIST_TYPE_INL