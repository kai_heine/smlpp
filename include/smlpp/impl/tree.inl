#ifndef SMLPP_IMPL_TREE_INL
#define SMLPP_IMPL_TREE_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
optional<tree> parse_tree(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (tl.length != 3) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	tree t;

	t.parameter_name = parse_octet_string(begin, end).value_or(octet_string{});
	t.parameter_value = parse_proc_par_value(begin, end);

	auto child_list_tl = parse_type_length(begin, end);
	if (child_list_tl == impl::optional_type_length) {
		t.child_list = std::nullopt;
		return t;
	}
	if (child_list_tl.type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	sequence<tree> child_list;

	for (std::size_t i = 0; i < child_list_tl.length; ++i) {
		child_list.push_back(parse_tree(begin, end).value());
	}

	t.child_list = child_list;

	return t;
}

template <typename OutputIterator>
OutputIterator serialize_tree(const tree& t, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 3}, dest);

	if (t.parameter_name.empty()) {
		throw std::system_error(errc::octet_string_empty_but_not_optional, __func__);
	}
	dest = serialize_octet_string(t.parameter_name, dest);
	dest = t.parameter_value ? serialize_proc_par_value(*t.parameter_value, dest)
	                         : serialize_optional(dest);
	if (!t.child_list) {
		return serialize_optional(dest);
	}

	dest = serialize_type_length({type_length::list_of_type, t.child_list->size()}, dest);
	for (const auto& subtree : *t.child_list) {
		dest = serialize_tree(subtree, dest);
	}
	return dest;
}

template <typename InputIterator>
tree_path parse_tree_path(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}

	tree_path tp;
	for (std::size_t i = 0; i < length; ++i) {
		tp.path_entries.push_back(parse_octet_string(begin, end).value());
	}
	return tp;
}

template <typename OutputIterator>
OutputIterator serialize_tree_path(const tree_path& t, OutputIterator dest) {
	dest = serialize_type_length({type_length::list_of_type, t.path_entries.size()}, dest);
	for (const auto& entry : t.path_entries) {
		dest = serialize_octet_string(entry, dest);
	}
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_TREE_INL