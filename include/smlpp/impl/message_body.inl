#ifndef SMLPP_IMPL_MESSAGE_BODY_INL
#define SMLPP_IMPL_MESSAGE_BODY_INL

#include "../error.h"
#include "../integer.h"
#include "../type_length_field.h"

namespace sml {

namespace impl {
enum class message_body_tag : unsigned32 {
	public_open_request = 0x0100,
	public_open_response = 0x0101,
	public_close_request = 0x0200,
	public_close_response = 0x0201,
	get_profile_pack_request = 0x0300,
	get_profile_pack_response = 0x0301,
	get_profile_list_request = 0x0400,
	get_profile_list_response = 0x0401,
	get_proc_parameter_request = 0x0500,
	get_proc_parameter_response = 0x0501,
	set_proc_parameter_request = 0x0600,
	get_list_request = 0x0700,
	get_list_response = 0x0701,
	get_cosem_request = 0x0800,
	get_cosem_response = 0x0801,
	set_cosem_request = 0x0900,
	set_cosem_response = 0x0901,
	action_cosem_request = 0x0a00,
	action_cosem_response = 0x0a01,
	attention_response = 0xFF01
};
}

template <typename InputIterator>
message_body parse_message_body(InputIterator& begin, InputIterator end) {
	auto [type, length] = parse_type_length(begin, end);
	if (type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	using impl::message_body_tag;
	auto tag = parse_integer<message_body_tag>(begin, end).value();
	switch (tag) {
	case message_body_tag::public_open_request:
		return parse_public_open_request(begin, end);
	case message_body_tag::public_open_response:
		return parse_public_open_response(begin, end);
	case message_body_tag::public_close_request:
		return parse_public_close_request(begin, end);
	case message_body_tag::public_close_response:
		return parse_public_close_response(begin, end);
	case message_body_tag::get_proc_parameter_request:
		return parse_get_proc_parameter_request(begin, end);
	case message_body_tag::get_proc_parameter_response:
		return parse_get_proc_parameter_response(begin, end);
	case message_body_tag::set_proc_parameter_request:
		return parse_set_proc_parameter_request(begin, end);
	case message_body_tag::attention_response:
		return parse_attention_response(begin, end);
	case message_body_tag::get_list_response:
		return parse_get_list_response(begin, end);
	default:
		throw std::system_error(errc::unsupported_message_body_choice, __func__);
	}
}

template <typename OutputIterator>
OutputIterator serialize_message_body(const message_body& m, OutputIterator dest) {
	using namespace impl;
	dest = serialize_type_length({type_length::list_of_type, 2}, dest);
	return std::visit(
	    visitor{

	        [&](const public_open_request& p) {
		        dest = serialize_integer(enum_val(message_body_tag::public_open_request), dest);
		        return serialize_public_open_request(p, dest);
	        },
	        [&](const public_open_response& p) {
		        dest = serialize_integer(enum_val(message_body_tag::public_open_response), dest);
		        return serialize_public_open_response(p, dest);
	        },
	        [&](const public_close_request& p) {
		        dest = serialize_integer(enum_val(message_body_tag::public_close_request), dest);
		        return serialize_public_close_request(p, dest);
	        },
	        [&](const public_close_response& p) {
		        dest = serialize_integer(enum_val(message_body_tag::public_close_response), dest);
		        return serialize_public_close_response(p, dest);
	        },
	        [&](const get_proc_parameter_request& p) {
		        dest =
		            serialize_integer(enum_val(message_body_tag::get_proc_parameter_request), dest);
		        return serialize_get_proc_parameter_request(p, dest);
	        },
	        [&](const get_proc_parameter_response& p) {
		        dest = serialize_integer(enum_val(message_body_tag::get_proc_parameter_response),
		                                 dest);
		        return serialize_get_proc_parameter_response(p, dest);
	        },
	        [&](const set_proc_parameter_request& p) {
		        dest =
		            serialize_integer(enum_val(message_body_tag::set_proc_parameter_request), dest);
		        return serialize_set_proc_parameter_request(p, dest);
	        },
	        [&](const attention_response& p) {
		        dest = serialize_integer(enum_val(message_body_tag::attention_response), dest);
		        return serialize_attention_response(p, dest);
	        },
	        [&](const get_list_response& p) {
		        dest = serialize_integer(enum_val(message_body_tag::get_list_response), dest);
		        return serialize_get_list_response(p, dest);
	        },
	        [&](auto) -> OutputIterator {
		        throw std::system_error(errc::unsupported_message_body_choice, __func__);
	        }},
	    m);
}

} // namespace sml

#endif // SMLPP_IMPL_MESSAGE_BODY_INL