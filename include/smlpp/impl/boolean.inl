#ifndef SMLPP_IMPL_BOOLEAN_INL
#define SMLPP_IMPL_BOOLEAN_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

namespace impl {
constexpr std::uint8_t BOOLEAN_TRUE{0xff};
constexpr std::uint8_t BOOLEAN_FALSE{0x00};
} // namespace impl

template <typename InputIterator>
optional<boolean> parse_boolean(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::boolean_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (tl.length != 1) {
		throw std::system_error(errc::unexpected_length, __func__);
	}
	if (std::distance(begin, end) < static_cast<std::ptrdiff_t>(tl.length)) {
		throw std::system_error(errc::short_buffer, __func__);
	}

	boolean b = *begin != impl::BOOLEAN_FALSE;
	++begin;
	return b;
}

template <typename OutputIterator>
OutputIterator serialize_boolean(boolean b, OutputIterator dest) {
	using namespace impl;

	dest = serialize_type_length({type_length::boolean_type, 1}, dest);
	*dest++ = b ? BOOLEAN_TRUE : BOOLEAN_FALSE;
	return dest;
}

} // namespace sml

#endif // SMLPP_IMPL_BOOLEAN_INL