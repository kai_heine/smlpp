#ifndef SMLPP_IMPL_TYPES_INL
#define SMLPP_IMPL_TYPES_INL

namespace sml::impl {

template <typename T, typename Parameter>
class strong_type {
public:
	explicit strong_type(T const& value) : value_(value) {}
	explicit strong_type(T&& value) : value_(std::move(value)) {}
	T& get() { return value_; }
	T const& get() const { return value_; }

private:
	T value_;
};

template <typename Enum, typename = std::enable_if_t<std::is_enum_v<Enum>>>
constexpr auto enum_val(Enum e) noexcept {
	return static_cast<std::underlying_type_t<Enum>>(e);
}

template <class... Ts>
struct visitor : Ts... {
	using Ts::operator()...;
};
template <class... Ts>
visitor(Ts...)->visitor<Ts...>;

} // namespace sml::impl

#endif // SMLPP_IMPL_TYPES_INL