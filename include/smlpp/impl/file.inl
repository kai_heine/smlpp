#ifndef SMLPP_IMPL_FILE_INL
#define SMLPP_IMPL_FILE_INL

#include "../crc16.h"
#include "../error.h"
#include <algorithm>
#include <array>
#include <cassert>

namespace sml {

namespace impl {
inline constexpr std::uint8_t zero_padding{0x00};
inline constexpr std::array<std::uint8_t, 8> start_seq{0x1b, 0x1b, 0x1b, 0x1b,
                                                       0x01, 0x01, 0x01, 0x01};
inline constexpr std::array<std::uint8_t, 5> end_seq{0x1b, 0x1b, 0x1b, 0x1b, 0x1a};
} // namespace impl

template <bool use_transport_protocol, typename InputIterator>
file parse_file(InputIterator begin, InputIterator end) {
	using namespace impl;

	assert(begin < end);

	[[maybe_unused]] auto old_begin = begin;

	// try to find start sequence
	if constexpr (use_transport_protocol) {
		if (std::distance(begin, end) < 8) {
			throw std::system_error(errc::short_buffer, __func__);
		}
		if (!std::equal(begin, begin + start_seq.size(), start_seq.begin(), start_seq.end())) {
			throw std::system_error(errc::invalid_transport_protocol, __func__);
		}
		begin += start_seq.size();
	}

	// parse file
	file f;
	while (begin != end) {
		if (*begin == zero_padding) {
			++begin;
		} else if (*begin == end_seq.front()) {
			break;
		} else {
			f.messages.push_back(parse_message(begin, end));
		}
	}

	// try to find end sequence
	if constexpr (use_transport_protocol) {
		if (std::distance(begin, end) < 8) {
			throw std::system_error(errc::short_buffer, __func__);
		}
		if (!std::equal(begin, begin + end_seq.size(), end_seq.begin(), end_seq.end())) {
			throw std::system_error(errc::invalid_transport_protocol, __func__);
		}
		begin += end_seq.size() + 1; // advance to crc
		std::uint16_t crc = (begin[0] << 8) | (begin[1]);
		if (crc != crc16(old_begin, begin)) {
			throw std::system_error(errc::incorrect_crc, __func__);
		}
		begin += 2;
	}

	return f;
}

template <bool use_transport_protocol>
std::vector<std::uint8_t> serialize_file(const file& f) {
	using namespace impl;

	std::vector<std::uint8_t> tmp;
	auto tmp_dest = std::back_inserter(tmp);

	// serialize start sequence
	if constexpr (use_transport_protocol) {
		std::copy(start_seq.begin(), start_seq.end(), tmp_dest);
	}

	// serialize messages
	for (const auto& message : f.messages) {
		serialize_message(message, tmp_dest);
	}

	// serialize end sequence
	if constexpr (use_transport_protocol) {
		std::uint8_t padding_len = tmp.size() % 4;
		tmp.insert(tmp.end(), padding_len, zero_padding);
		std::copy(end_seq.begin(), end_seq.end(), tmp_dest);
		tmp.push_back(padding_len);
		auto crc = crc16(tmp.begin(), tmp.end());
		tmp.push_back(crc >> 8);
		tmp.push_back(crc & 0xff);
	}

	return tmp;
}

template <bool use_transport_protocol, typename OutputIterator>
OutputIterator serialize_file(const file& f, OutputIterator dest) {
	auto tmp = serialize_file<use_transport_protocol>(f);
	return std::copy(tmp.begin(), tmp.end(), dest);
}

} // namespace sml

#endif // SMLPP_IMPL_FILE_INL