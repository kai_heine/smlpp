#ifndef SMLPP_IMPL_TIME_INL
#define SMLPP_IMPL_TIME_INL

#include "../error.h"
#include "../type_length_field.h"

namespace sml {

template <typename InputIterator>
optional<timestamp> parse_timestamp(InputIterator& begin, InputIterator end) {
	return optional<timestamp>{parse_integer<unsigned32>(begin, end)};
}

template <typename InputIterator>
optional<timestamp_local> parse_timestamp_local(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (tl.length != 3) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	auto time_stamp = parse_timestamp(begin, end).value();
	auto local_offset = parse_integer<integer16>(begin, end).value();
	auto season_time_offset = parse_integer<integer16>(begin, end).value();

	return timestamp_local{time_stamp, local_offset, season_time_offset};
}

template <typename OutputIterator>
OutputIterator serialize_timestamp_local(const timestamp_local& t, OutputIterator dest) {
	dest = serialize_type_length({type_length::list_of_type, 3}, dest);
	dest = serialize_integer(t.time_stamp.get(), dest);
	dest = serialize_integer(t.local_offset, dest);
	dest = serialize_integer(t.season_time_offset, dest);
	return dest;
}

namespace impl {
enum class time_tag : unsigned8 { sec_index = 0x01, timestamp = 0x02, local_timestamp = 0x03 };
}

template <typename InputIterator>
optional<time> parse_time(InputIterator& begin, InputIterator end) {
	auto tl = parse_type_length(begin, end);
	if (tl == impl::optional_type_length) {
		return std::nullopt;
	}
	if (tl.type != type_length::list_of_type) {
		throw std::system_error(errc::unexpected_type, __func__);
	}
	if (tl.length != 2) {
		throw std::system_error(errc::unexpected_length, __func__);
	}

	using impl::time_tag;
	auto tag = parse_integer<time_tag>(begin, end).value();
	switch (tag) {
	case time_tag::sec_index:
		return parse_integer<unsigned32>(begin, end);
	case time_tag::timestamp:
		return parse_timestamp(begin, end);
	case time_tag::local_timestamp:
		return parse_timestamp_local(begin, end);
	default:
		throw std::system_error(errc::unsupported_time_choice, __func__);
	}
}

template <typename OutputIterator>
OutputIterator serialize_time(const time& t, OutputIterator dest) {
	using namespace impl;

	dest = serialize_type_length({type_length::list_of_type, 2}, dest);
	return std::visit(
	    visitor{

	        [&](unsigned32 sec_index) {
		        dest = serialize_integer(enum_val(time_tag::sec_index), dest);
		        return serialize_integer(sec_index, dest);
	        },
	        [&](timestamp time_stamp) {
		        dest = serialize_integer(enum_val(time_tag::timestamp), dest);
		        return serialize_integer(time_stamp.get(), dest);
	        },
	        [&](const timestamp_local& local_timestamp) {
		        dest = serialize_integer(enum_val(time_tag::local_timestamp), dest);
		        return serialize_timestamp_local(local_timestamp, dest);
	        }

	    },
	    t);
}

} // namespace sml

#endif // SMLPP_IMPL_TIME_INL
