#ifndef SMLPP_CRC16_H
#define SMLPP_CRC16_H

#include "integer.h"

namespace sml {

template <typename InputIterator>
constexpr unsigned16 crc16(InputIterator begin, InputIterator end) noexcept;

} // namespace sml

#include "impl/crc16.inl"

#endif // SMLPP_CRC16_H