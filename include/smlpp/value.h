#ifndef SMLPP_VALUE_H
#define SMLPP_VALUE_H

#include "boolean.h"
#include "integer.h"
#include "list_type.h"
#include "octet_string.h"

namespace sml {

using value = implicit_choice<boolean, octet_string, integer8, integer16, integer32, integer64,
                              unsigned8, unsigned16, unsigned32, unsigned64, list_type>;

template <typename InputIterator>
value parse_value(InputIterator& begin, InputIterator end);

template <typename OutputIterator>
OutputIterator serialize_value(const value& v, OutputIterator dest);

} // namespace sml

#include "impl/value.inl"

#endif // SMLPP_VALUE_H